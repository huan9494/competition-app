class Contest < ApplicationRecord

  mount_uploader :picture, FileUploader
  validates :name, presence: true
  validates :description, presence: true
  validates :picture, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  has_many :user_contests
  has_many :user, through: :user_contests
end
