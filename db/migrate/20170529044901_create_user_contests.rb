class CreateUserContests < ActiveRecord::Migration[5.0]
  def change
    create_table :user_contests do |t|
      t.references :user, foreign_key: true
      t.references :contest, foreign_key: true
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
